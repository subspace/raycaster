#include "draw.h"
#include <iostream>
#include <math.h>

Draw::Draw()
{
}

bool adjustVerticalBounds(SDL_Surface* scr, int x, int &y1, int &y2)
{
    if(y2 < y1) {
        y1 += y2;
        y2 = y1 - y2;
        y1 -= y2;
    }
    if(y2 < 0 || y1 >= scr->h  || x < 0 || x >= scr->w)
        return false;

    if(y1 < 0)
        y1 = 0;
    if(y2 >= scr->h)
        y2 = scr->h - 1;

    return true;
}

void verticalLine(SDL_Surface* scr, int x, int y1, int y2, Uint32 color)
{
    if(!adjustVerticalBounds(scr, x, y1, y2))
        return;
    Uint32* bufp;
    bufp = (Uint32*)scr->pixels + x + (y1 * scr->pitch / 4);
    for(int y = y1; y <= y2; y++) {
        *bufp = color;
        bufp += scr->pitch / 4;
    }
}

void Draw::wall(SDL_Surface* scr, int x, int height, Uint32 color)
{
    int y1 = scr->h/2 - height/2;
    int y2 = scr->h/2 + height/2;
    verticalLine(scr, x, y1, y2, color);
}

void Draw::ceiling(SDL_Surface *scr, int x, int height, Uint32 color)
{
    int y1 = 0;
    int y2 = scr->h/2 - height/2;
    verticalLine(scr, x, y1, y2, color);
}

void Draw::floor(SDL_Surface *scr, int x, int height, Uint32 color)
{
    int y1 = scr->h/2 + height/2;
    int y2 = scr->h;
    verticalLine(scr, x, y1, y2, color);
}

void Draw::wallTexture(SDL_Surface *scr, int x, int height, SDL_Surface *texture, int u, int screenHalfHeight)
{
    int y1 = screenHalfHeight - height/2;
    int y2 = screenHalfHeight + height/2;
    if(!adjustVerticalBounds(scr, x, y1, y2))
        return;

    Uint32* bufp;
    Uint32* texbuf;
    texbuf = (Uint32*)texture->pixels;
    bufp = (Uint32*)scr->pixels + x + (y1 * scr->pitch / 4);
    int v;
    for(int y = y1; y <= y2; y++) {
        v = ::floor(((y-screenHalfHeight) + height/2.0f)*(float(texture->h)/height));
        *bufp = texbuf[u + (v * texture->pitch/4)];
        bufp += scr->pitch / 4;
    }
}
