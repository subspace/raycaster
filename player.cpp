#include "raycaster.h"
#include "player.h"
#include "mathlib.h"
#include <math.h>

Player::Player(float x, float y, float angle):
    moveSpeed_(64),
    yawSpeed_(180)
{
    position_.setCoords(x, y);
    float a = MathLib::deg2rad(angle);
    direction_.setX(cos(a));
    direction_.setY(sin(a));
    rotate(0);
    positionInMap_.setCoords(x/Raycaster::mapBlockSize(), y/Raycaster::mapBlockSize());
}

void Player::rotate(float angle)
{
    // Apply the rotation matrix transformation to both camera plane vector and player
    // direction vector:
    MathLib::rotate(direction_, angle);

    cameraPlane_.setCoords(direction_.y(), -direction_.x());
}

void Player::move(float speed)
{
    position_ += direction()*speed;
}

void Player::setX(float x) { position_.setX(x), positionInMap_.setX(int(x/Raycaster::mapBlockSize())); }
void Player::setY(float y) { position_.setY(y), positionInMap_.setY(int(y/Raycaster::mapBlockSize())); }
