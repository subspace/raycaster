#ifndef POINT2I_H
#define POINT2I_H

class Point2i
{
private:
    int x_, y_;
public:
    Point2i(int x = 0, int y = 0);
    void setX(int x) { x_ = x; }
    void setY(int y) { y_ = y; }
    void setCoords(int x, int y) { setX(x), setY(y); }
    int x() const { return x_; }
    int y() const { return y_; }
};

#endif // POINT2I_H
