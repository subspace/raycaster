#ifndef VECTOR2F_H
#define VECTOR2F_H

class Vector2f
{
private:
    float x_, y_;

public:
    Vector2f(float x=0, float y=0):
        x_(x),
        y_(y)
    {}

    void normalize();
    Vector2f normalized() const;
    float magnitude() const;
    void setCoords(float x, float y)
    {
        x_ = x, y_ = y;
    }
    void setX(float x) { x_ = x; }
    void setY(float y) { y_ = y; }
    float x() const { return x_; }
    float y() const { return y_; }

    Vector2f& operator*=(float scalar)
    {
        x_ *= scalar;
        y_ *= scalar;
        return *this;
    }

    Vector2f& operator/=(float scalar)
    {
        x_ /= scalar;
        y_ /= scalar;
        return *this;
    }

    Vector2f& operator+=(const Vector2f& v)
    {
        setX(v.x() + x());
        setY(v.y() + y());
        return *this;
    }

    Vector2f operator+(const Vector2f& v) const
    {
        return Vector2f(x()+v.x(), y()+v.y());
    }

    Vector2f operator-(const Vector2f& v) const
    {
        return Vector2f(x()-v.x(), y()-v.y());
    }

    Vector2f operator*(float scalar) const
    {
        return Vector2f(x()*scalar, y()*scalar);
    }

    static float dotProduct(const Vector2f& v1, const Vector2f& v2)
    {
        return (v1.x()*v2.x() + v1.y()*v2.y());
    }

    Vector2f operator/(const Vector2f& v) const
    {
        return Vector2f(this->x()/v.x(), this->y()/v.y());
    }
};

#endif // VECTOR2F_H
