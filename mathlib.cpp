#include "mathlib.h"
#include "vector2f.h"

float MathLib::deg2rad(float angle)
{
    return angle*M_PI/180;
}

void MathLib::rotate(Vector2f &vec, float angle)
{
    // [ cos(a) -sin(a) ][ dirX ]
    // [ sin(a)  cos(a) ][ dirY ]
    float a = MathLib::deg2rad(angle);
    double sinA = sin(a);
    double cosA = cos(a);

    Vector2f dir(vec);
    vec.setX(dir.x()*cosA - dir.y()*sinA);
    vec.setY(dir.x()*sinA + dir.y()*cosA);
}

MathLib::MathLib()
{
}
