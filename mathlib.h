#ifndef MATHLIB_H
#define MATHLIB_H

#include <math.h>

class Vector2f;

class MathLib
{
public:
    static float deg2rad(float angle);
    static void rotate(Vector2f& vec, float angle);
    template<class T> static const T& min(const T& a, const T& b) { return (a < b) ? a : b; }
    template<class T> static const T& max(const T& a, const T& b) { return (a > b) ? a : b; }

private:
    MathLib();
};

#endif // MATHLIB_H
