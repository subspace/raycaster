#ifndef RAYCASTER_H
#define RAYCASTER_H

#include <SDL/SDL.h>
#include "player.h"

class Raycaster
{
public:
    Raycaster();
    int exec();
    static int screenWidth() { return ScreenWidth_; }
    static int screenHeight() { return ScreenHeight_; }
    static int mapBlockSize() { return MapBlockSize_; }

private:
    static const int ScreenWidth_ = 640;
    static const int ScreenHeight_ = 480;
    static const int ScreenHalfHeight_ = ScreenHeight_/2;
    static const int ScreenHalfWidth_ = ScreenWidth_/2;
    static const int MapBlockSize_ = 16;

    int FOV_;
    SDL_Surface* screen_;
    SDL_Surface* textures_[5];

    enum { CollisionXAxis, CollisionYAxis };

    static Uint32 colors_[3];
    bool running_;
    Player player_;
    enum KeyboardState { UpPressed = 1<<0, DownPressed = 1<<1, LeftPressed = 1<<2, RightPressed = 1<<3, LookUpPressed = 1<<4, LookDownPressed = 1<<5 };
    int keyState_;
    int playerHeight_;

    bool initialize();
    void buildTables();
    void updatePlayers(double frameTime);
    void render();
    bool loadTexture(SDL_Surface **out, const char* file);
    void cleanup();

    /* Events */
    void onEvent(SDL_Event* evt);
    void onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
    void onKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);
    void onExit();
};

#endif // RAYCASTER_H
