#ifndef POINT2F_H
#define POINT2F_H

class Point2f
{
public:
    Point2f(float x, float y);
    void setCoords(float x, float y) { x_ = x, y_ = y; }
    void setX(float x) { x_ = x; }
    void setY(float y) { y_ = y; }
    float x() const { return x_; }
    float y() const { return y_; }

private:
    float x_, y_;

};

#endif // POINT2F_H
