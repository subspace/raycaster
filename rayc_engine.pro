TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Vector2f.cpp \
    raycaster.cpp \
    point2i.cpp \
    mathlib.cpp \
    player.cpp \
    draw.cpp \
    point2f.cpp

HEADERS += \
    Vector2f.h \
    raycaster.h \
    vector2f.h \
    point2i.h \
    mathlib.h \
    player.h \
    map.h \
    draw.h \
    point2f.h


unix|win32: LIBS += -lSDL

unix|win32: LIBS += -lSDL_image

OTHER_FILES += \
    textures/glowstone.png \
    textures/cobblestone.png \
    textures/brick.png \
    textures/iron_block.png \
    textures/log_spruce_top.png
