#include "vector2f.h"
#include <math.h>

float Vector2f::magnitude() const
{
    return sqrtf(x()*x() + y()*y());
}

void Vector2f::normalize()
{
    float mag = magnitude();
    x_ /= mag;
    y_ /= mag;
}

Vector2f Vector2f::normalized() const
{
    float mag = magnitude();
    return *this/mag;
}
