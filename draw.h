#ifndef DRAW_H
#define DRAW_H

#include <SDL/SDL.h>

class Draw
{
public:
    static void wall(SDL_Surface *scr, int x, int height, Uint32 color);
    static void ceiling(SDL_Surface *scr, int x, int height, Uint32 color);
    static void floor(SDL_Surface *scr, int x, int height, Uint32 color);
    static void wallTexture(SDL_Surface* scr, int x, int height, SDL_Surface* texture, int u, int screenHalfHeight);
private:
    Draw();
};

#endif // DRAW_H
