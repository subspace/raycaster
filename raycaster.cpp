#include "raycaster.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "mathlib.h"
#include "map.h"
#include "draw.h"
#include <iostream>
#include <unistd.h>

using namespace std;
Uint32 Raycaster::colors_[3];

Raycaster::Raycaster():
    FOV_(80),
    running_(false),
    player_(MapBlockSize_*3, MapBlockSize_*3, 180),
    keyState_(0)
{
}

void Raycaster::cleanup()
{
    for(int i = 0; i < 3; ++i) {
        SDL_FreeSurface(textures_[i]);
    }
    SDL_FreeSurface(screen_);
}

bool Raycaster::loadTexture(SDL_Surface **out, const char *file)
{
    SDL_Surface* tmp = IMG_Load(file);
    if(!tmp)
        return false;
    *out = SDL_DisplayFormat(tmp);
    SDL_FreeSurface(tmp);
    return true;
}

bool Raycaster::initialize()
{
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
        return false;
    screen_ = SDL_SetVideoMode(ScreenWidth_, ScreenHeight_, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if(!screen_)
        return false;
    SDL_WM_SetCaption("Mihaycaster", NULL);
    colors_[0] = SDL_MapRGB(screen_->format, 0, 0, 255);
    colors_[1] = SDL_MapRGB(screen_->format, 0, 255, 0);
    colors_[2] = SDL_MapRGB(screen_->format, 255, 0, 0);
    if(!loadTexture(&textures_[0], "textures/brick.png") ||
       !loadTexture(&textures_[1], "textures/cobblestone.png") ||
       !loadTexture(&textures_[2], "textures/glowstone.png") ||
       !loadTexture(&textures_[3], "textures/iron_block.png") ||
       !loadTexture(&textures_[4], "textures/log_spruce_top.png"))
        return false;


    playerHeight_ = ScreenHeight_/2;
    return true;
}

int Raycaster::exec()
{
    if(!initialize())
        return 1;
    running_ = true;

    double time, oldTime, newTime;
    double extraTime, minFrameTime, trueFrameTime, frameTime;
    oldTime = SDL_GetTicks() / 1000.0;
    extraTime = 0.001;
    minFrameTime = 1/77.0;

    while(running_) {
        // Frame timing
        newTime = SDL_GetTicks() / 1000.0;
        time = newTime - oldTime;
        oldTime = newTime;

        extraTime += time;
        if(extraTime < minFrameTime) {
            usleep((minFrameTime-extraTime)*1000*1000);
            continue;
        }

        trueFrameTime = extraTime - 0.001;
        trueFrameTime = MathLib::min<double>(trueFrameTime, minFrameTime);
        extraTime -= trueFrameTime;
        frameTime = MathLib::min<double>(0.2, trueFrameTime);
//        cout << 1/frameTime << endl;

        SDL_Event evt;
        while(SDL_PollEvent(&evt)) {
            onEvent(&evt);
        }

        updatePlayers(time);
        render();
    }

    cleanup();

    return 0;
}

void Raycaster::updatePlayers(double frameTime)
{
    if(!keyState_)
        return;

    float moveSpeed = frameTime * player_.moveSpeed();
    float yawSpeed = frameTime * player_.yawSpeed();
    float newX, newY;
    int mapX, mapY;

    if(keyState_ & UpPressed) {
        newX = player_.position().x() + player_.direction().x()*moveSpeed;
        newY = player_.position().y() + player_.direction().y()*moveSpeed;
        mapX = int(newX / MapBlockSize_);
        mapY = int(newY / MapBlockSize_);
        if(!map[mapX][player_.positionInMap().y()])
            player_.setX(newX);
        if(!map[player_.positionInMap().x()][mapY])
            player_.setY(newY);
    }
    if(keyState_ & DownPressed) {
        newX = player_.position().x() - player_.direction().x()*moveSpeed;
        newY = player_.position().y() - player_.direction().y()*moveSpeed;
        mapX = int(newX / MapBlockSize_);
        mapY = int(newY / MapBlockSize_);
        if(!map[mapX][player_.positionInMap().y()])
            player_.setX(newX);
        if(!map[player_.positionInMap().x()][mapY])
            player_.setY(newY);
    }
    if(keyState_ & LeftPressed)
        player_.rotate(yawSpeed);
    if(keyState_ & RightPressed)
        player_.rotate(-yawSpeed);
}

void Raycaster::render()
{
    if(SDL_MUSTLOCK(screen_))
        SDL_LockSurface(screen_);

    float projectionDistance = (ScreenWidth_/2.0f) / tan(MathLib::deg2rad(FOV_/2.0f));
    float fovDelimiter = FOV_/90.0;
    for(int x = 0; x < ScreenWidth_; ++x) {
        // Setup the ray direction
        // The camera plane is the orthogonal complement of the player direction this is setup
        // at the Player class each and every rotation or the player.
        float scalar = -1 + (x/float(ScreenWidth_))*2;
        scalar *= fovDelimiter;
        Vector2f rayDirection(Vector2f(player_.direction() + player_.cameraPlane()*scalar));
        rayDirection.normalize();

#if 0
        // Arc length per x/y step using the Pythagorean theorem:
        // ds^2 = dx^2 + dy^2
        // (ds/dx)^2 = 1 + (dy/dx)^2
        // ds/dx = sqrt(1 + (dy/dx)^2)
        // To get the arc length per y step this divide the whole equation by dy
        // on the second step
        float arcLenghtPerXStep = sqrt(1+(rayDirection.y()*rayDirection.y())/(rayDirection.x()*rayDirection.x()))*MapBlockSize_;
        float arcLenghtPerYStep = sqrt(1+(rayDirection.x()*rayDirection.x())/(rayDirection.y()*rayDirection.y()))*MapBlockSize_;
#endif

        // using soh-cah-toa to find the length
        float theta;
        theta = atan(fabs(rayDirection.y()/rayDirection.x()));
        float arcLengthPerXStep = MapBlockSize_/cos(theta);
        float arcLengthPerYStep = MapBlockSize_/sin(theta);

        // Find the first x and y intercepts in the grid
        // yIntercept is where the ray crosses a vertical grid line
        // xIntercept is where the ray crosses a horizontal line
        // This can be seen as how much of a full arc length per x/y step I am from the next x/y intercept
        // that the ray is pointing to.
        // Distance = arcLengthPerStep * Ratio
        int mapX = player_.positionInMap().x();
        int mapY = player_.positionInMap().y();
        float yInterceptDist;
        int stepX;
        if(rayDirection.x() >= 0) {
            yInterceptDist = arcLengthPerXStep * ((mapX*MapBlockSize_ + MapBlockSize_ - player_.position().x())/MapBlockSize_);
            stepX = +1;
        } else {
            yInterceptDist = arcLengthPerXStep * ((player_.position().x() - mapX*MapBlockSize_)/MapBlockSize_);
            stepX = -1;
        }
        float xInterceptDist;
        int stepY;
        if(rayDirection.y() >= 0) {
            xInterceptDist = arcLengthPerYStep * ((mapY*MapBlockSize_ + MapBlockSize_ - player_.position().y())/MapBlockSize_);
            stepY = +1;
        } else {
            xInterceptDist = arcLengthPerYStep * ((player_.position().y() - mapY*MapBlockSize_)/MapBlockSize_);
            stepY = -1;
        }

        // After finding the first intersection points, we can search for a wall
        // jumping from intersection to intersection.
        int collidedWith;
        float rayLength;
        for(;;) {
            if(yInterceptDist < xInterceptDist) {
                mapX += stepX;
                if(map[mapX][mapY]) {
                    collidedWith = CollisionYAxis;
                    rayLength = yInterceptDist;
                    break;
                }
                yInterceptDist += arcLengthPerXStep;
            } else {
                mapY += stepY;
                if(map[mapX][mapY]) {
                    collidedWith = CollisionXAxis;
                    rayLength = xInterceptDist;
                    break;
                }
                xInterceptDist += arcLengthPerYStep;
            }
        }

        // Draw the vertical line
        // we don't use the actual distance found, 'cos it would give you the fisheye effect since the rays
        // casted that aren't collinear with the player direction have a larger length
        // So to fix that we get the actual distance from the player to the wall to create the projection
        float wallDistance;
        float cosTheta = Vector2f::dotProduct(player_.direction(), rayDirection) / rayDirection.magnitude();
        wallDistance = cosTheta * rayLength;

#if 0
        // 2nd method ratio based
        // CorrectDistanceFromWall/PlayerDistanceFromWall = CorrectDistanceFromCameraPlane/PlayerDistanceFromCameraPlane
        //
        if(collidedWith == CollisionYAxis)
            wallDistance = fabs((mapX*MapStepLength_ - player_.position().x() + (MapStepLength_-stepX*MapStepLength_)/2) / rayDirection.x());
        else
            wallDistance = fabs((mapY*MapStepLength_ - player_.position().y() + (MapStepLength_-stepY*MapStepLength_)/2) / rayDirection.y());
#endif

        // Setup projection otherwise the walls will be seen far off, fov wont work correctly either:
        // PlayerDistanceFromProjectionPlane / ProjectedWallHeight = PlayerDistanceFromWall / WallHeight
        int projectedHeight;
        projectedHeight = MapBlockSize_/wallDistance*projectionDistance;

        // For moving the projection plane, make it look like we are looking up and down
        int halfScreenHeight;
        if(keyState_ & LookUpPressed)
            halfScreenHeight = ScreenHalfHeight_/4;
        else if(keyState_ & LookDownPressed)
            halfScreenHeight = ScreenHalfHeight_*1.8;
        else
            halfScreenHeight = ScreenHalfHeight_;

        // Draw Wall texture
        // Find the texture u coordinate
        SDL_Surface* texture = textures_[map[mapX][mapY] - 1];
        int texU, texV;
        Vector2f ray(rayDirection * rayLength);
        if(collidedWith == CollisionXAxis) {
            float rayX = ray.x() + player_.position().x();
            texU = int((rayX - mapX*MapBlockSize_)/MapBlockSize_ * texture->w);
        } else {
            float rayY = ray.y() + player_.position().y();
            texU = int((rayY - mapY*MapBlockSize_)/MapBlockSize_ * texture->w);
        }
        Draw::wallTexture(screen_, x, projectedHeight, texture, texU, halfScreenHeight);

        // Draw floor & ceiling
        int wallProjectionStart = halfScreenHeight - projectedHeight/2;
        int wallProjectionEnd = projectedHeight/2 + halfScreenHeight;
        float rayX, rayY;

        // Finding the floor pixel: straigthDistanceFromPixel/projectionDistance = playerheight/(y-playerHeight)
        for(int y = wallProjectionEnd; y < ScreenHeight_; ++y) {
            float straightDistanceFromFloorPixel = projectionDistance * (playerHeight_ / float(y+(ScreenHalfHeight_-halfScreenHeight) - playerHeight_)) * MapBlockSize_/ScreenHeight_;
            float distanceFromFloorPixel = straightDistanceFromFloorPixel / cosTheta;

            ray = rayDirection * distanceFromFloorPixel;
            ray += player_.position();
            rayX = ray.x() - floor(ray.x()/MapBlockSize_)*MapBlockSize_;
            rayY = ray.y() - floor(ray.y()/MapBlockSize_)*MapBlockSize_;
            texU = int(rayX/MapBlockSize_ * textures_[3]->w);
            texV = int(rayY/MapBlockSize_ * textures_[3]->h);
            ((Uint32*)screen_->pixels)[y * screen_->pitch/4 + x] = ((Uint32*)textures_[3]->pixels)[(texV * textures_[3]->pitch/4) + texU];
        }

        // ceiling
        for(int y = 0; y < wallProjectionStart; ++y) {
            float straightDistanceFromCeilingPixel = projectionDistance * (playerHeight_ / float(playerHeight_- y-(ScreenHalfHeight_-halfScreenHeight))) * MapBlockSize_/ScreenHeight_;
            float distanceFromCeilingPixel = straightDistanceFromCeilingPixel / cosTheta;

            ray = rayDirection * distanceFromCeilingPixel;
            ray += player_.position();
            rayX = ray.x() - floor(ray.x()/MapBlockSize_)*MapBlockSize_;
            rayY = ray.y() - floor(ray.y()/MapBlockSize_)*MapBlockSize_;
            texU = int(rayX/MapBlockSize_ * textures_[3]->w);
            texV = int(rayY/MapBlockSize_ * textures_[3]->h);
            ((Uint32*)screen_->pixels)[y * screen_->pitch/4 + x] = ((Uint32*)textures_[4]->pixels)[(texV * textures_[4]->pitch/4) + texU];
        }
    }

    if(SDL_MUSTLOCK(screen_))
        SDL_UnlockSurface(screen_);
    SDL_Flip(screen_);
}

void Raycaster::onKeyDown(SDLKey sym, SDLMod , Uint16 )
{
    switch(sym) {
    case SDLK_UP:
        keyState_ |= UpPressed;
        break;
    case SDLK_DOWN:
        keyState_ |= DownPressed;
        break;
    case SDLK_LEFT:
        keyState_ |= LeftPressed;
        break;
    case SDLK_RIGHT:
        keyState_ |= RightPressed;
        break;
    case SDLK_PAGEUP:
        FOV_-=10;
        cout << FOV_ << endl;
        break;
    case SDLK_PAGEDOWN:
        FOV_+=10;
        cout << FOV_ << endl;
        break;
    case SDLK_a:
        keyState_ |= LookUpPressed;
        break;
    case SDLK_z:
        keyState_ |= LookDownPressed;
        break;
    case SDLK_ESCAPE:
    case SDLK_q:
        onExit();
        break;
    default:
        break;
    }
}

void Raycaster::onKeyUp(SDLKey sym, SDLMod , Uint16 )
{
    switch(sym) {
    case SDLK_UP:
        keyState_ &= ~UpPressed;
        break;
    case SDLK_DOWN:
        keyState_ &= ~DownPressed;
        break;
    case SDLK_LEFT:
        keyState_ &= ~LeftPressed;
        break;
    case SDLK_RIGHT:
        keyState_ &= ~RightPressed;
        break;
    case SDLK_a:
        keyState_ &= ~LookUpPressed;
        break;
    case SDLK_z:
        keyState_ &= ~LookDownPressed;
        break;
    default:
        break;
    }
}

void Raycaster::onExit()
{
    running_ = false;
}

void Raycaster::onEvent(SDL_Event *evt)
{
    switch(evt->type) {
        case SDL_KEYDOWN:
            onKeyDown(evt->key.keysym.sym,evt->key.keysym.mod,evt->key.keysym.unicode);
            break;

        case SDL_KEYUP:
            onKeyUp(evt->key.keysym.sym,evt->key.keysym.mod,evt->key.keysym.unicode);
            break;

        case SDL_QUIT:
            onExit();
            break;

        default:
            break;
    }
}
