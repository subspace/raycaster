#ifndef PLAYER_H
#define PLAYER_H

#include "vector2f.h"
#include "point2i.h"


class Player
{
private:
    Vector2f position_;
    Vector2f direction_;
    Vector2f cameraPlane_;
    Point2i positionInMap_;
    float moveSpeed_;
    float yawSpeed_;

public:
    Player(float x = 0, float y = 0, float angle = 0);
    void rotate(float angle);
    void move(float speed);
    void setX(float x);
    void setY(float y);
    const Vector2f& direction() const { return direction_; }
    const Vector2f& position() const { return position_; }
    const Point2i& positionInMap() const { return positionInMap_; }
    const Vector2f& cameraPlane() const { return cameraPlane_; }
    float moveSpeed() const { return moveSpeed_; }
    float yawSpeed() const { return yawSpeed_; }
};

#endif // PLAYER_H
